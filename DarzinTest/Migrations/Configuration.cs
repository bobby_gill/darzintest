namespace DarzinTest.Migrations
{
    using DarzinTest.Models;
    using DarzinTest.DAL;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DBContext context)
        {
            var customers = new List<Customer>
            {
                new Customer { FirstName = "Carson",   LastName = "Alexander" },
                new Customer { FirstName = "Meredith", LastName = "Alonso" },
                new Customer { FirstName = "Arturo",   LastName = "Anand" },
                new Customer { FirstName = "Gytis",    LastName = "Barzdukas" },
                new Customer { FirstName = "Yan",      LastName = "Li" },
                new Customer { FirstName = "Peggy",    LastName = "Justice" },
                new Customer { FirstName = "Laura",    LastName = "Norman" },
                new Customer { FirstName = "Nino",     LastName = "Olivetto" }
            };


            customers.ForEach(s => context.Customers.AddOrUpdate(p => p.LastName, s));
            context.SaveChanges();
            
            var products = new List<Product>
            {
                new Product { Name = "Product 1", Price = 350000 },
                new Product { Name = "Product 2", Price = 100000 },
                new Product { Name = "Product 3", Price = 350000 },
                new Product { Name = "Product 4", Price = 100000 }
            };
            products.ForEach(s => context.Products.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var purchases = new List<Purchase>
            {
                new Purchase { CustomerID = customers.Single( s => s.LastName == "Alexander").ID,
                    ProductID = products.Single( s => s.Name == "Product 2").ID },
                new Purchase { CustomerID = customers.Single( s => s.LastName == "Anand").ID,
                    ProductID = products.Single( s => s.Name == "Product 3").ID },
            };
            purchases.ForEach(s => context.Purchases.AddOrUpdate(p => p.ID, s));
            context.SaveChanges();
        }
    }
}
