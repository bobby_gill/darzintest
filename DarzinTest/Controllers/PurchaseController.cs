﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DarzinTest.DAL;
using DarzinTest.Models;
using PagedList;
using System.Data.Entity.Infrastructure;
using DarzinTest.ViewModels;

namespace DarzinTest.Controllers
{
    public class PurchaseController : Controller
    {
        private DBContext db = new DBContext();

        // POST: Customer/Delete/{id}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Purchase purchase = db.Purchases.Find(id);
                db.Purchases.Remove(purchase);
                db.SaveChanges();
                return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (RetryLimitExceededException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return Json(new { result = "failed" }, JsonRequestBehavior.AllowGet);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
