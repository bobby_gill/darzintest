﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace DarzinTest.DAL
{
    public class DBConfiguration : DbConfiguration
    {
        public DBConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}