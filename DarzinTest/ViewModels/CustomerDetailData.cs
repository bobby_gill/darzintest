﻿using DarzinTest.Models;
using System.Collections.Generic;

namespace DarzinTest.ViewModels
{
    public class CustomerDetailData
    {
        public Customer customer { get; set; }
        public IEnumerable<Product> products { get; set; }
    }
}